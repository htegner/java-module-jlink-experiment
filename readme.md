# Compile source

Compile source into module structure:

`mkdir -p mods/com.greetings`
`javac -d mods/com.greetings/ src/com.greetings/module-info.java src/com.greetings/com/greetings/Greetings.java`

# Create custom JRE and launcher

Creates a custom JRE dist and launcher script in the `./out` folder:

`jlink --module-path mods --add-modules com.greetings --launcher greetings=com.greetings/com.greetings.Greetings --output out/`

